package com.site.spring.util;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class MyUtil {

	public static void flash(RedirectAttributes redirectAttributes,String kind, String message){
		redirectAttributes.addFlashAttribute("flashKind", kind);
		redirectAttributes.addFlashAttribute("flashMesssage", message);
	}
}

