package com.site.spring.services;

import com.site.spring.dto.SignupForm;

public interface UserService {

	public abstract void signup(SignupForm signupForm);
}
