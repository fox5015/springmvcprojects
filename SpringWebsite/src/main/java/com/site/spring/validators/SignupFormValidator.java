package com.site.spring.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.site.spring.dto.SignupForm;
import com.site.spring.entities.User;
import com.site.spring.repositories.UserRepository;


@Component
public class SignupFormValidator extends LocalValidatorFactoryBean {


	private UserRepository userRepository;
	
	
	@Autowired
	public SignupFormValidator(com.site.spring.repositories.UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return clazz.isAssignableFrom(SignupForm.class);
	}


	@Override
	public void validate(Object obj, Errors errors, final Object ...validationHints ) {
		super.validate(obj, errors, validationHints);
		
		if(!errors.hasErrors()){
			SignupForm signupForm = (SignupForm) obj;
			User user = userRepository.findByEmail(signupForm.getEmail());
			if(user !=null){
				errors.rejectValue("email", "emailNotUnique");
			}
		}
	}


	
}
