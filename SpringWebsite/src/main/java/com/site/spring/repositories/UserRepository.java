package com.site.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.site.spring.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
}
